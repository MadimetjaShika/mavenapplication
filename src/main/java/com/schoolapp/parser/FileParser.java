package com.schoolapp.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * This class defines all the file parsing functionality
 */
public class FileParser {

    /**
     * Parses a file line by line.
     *
     * @param fileName The path of the file to be read.
     * @return Returns an array list housing the contents of a given file, with each line at a new index.
     */
    public ArrayList parseFileByLine(String fileName) {

        if (new File(fileName).isFile()) {
            FileReader rd = null;

            try {
                File file = new File(fileName);
                ArrayList parseByLineResult = new ArrayList();
                rd = new FileReader(file);
                BufferedReader bf = new BufferedReader(rd);
                String temp = bf.readLine();
                while (temp != null) {
                    parseByLineResult.add(temp.toUpperCase());
                    temp = bf.readLine();
                }
                return parseByLineResult;

            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            } finally {
                try {
                    if (rd != null) {
                        rd.close();
                    }
                } catch (Exception ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
        return null;
    }
}
