package com.schoolapp.exceptions;

/**
 * Created by Madimetja on 2015/10/11.
 * Custom Exception class to throw exceptions when an employee object is not found.
 */
public class CustomFileHandlingException extends Exception {
    public CustomFileHandlingException(String message) {
        super(message);
    }
}