package com.schoolapp.app;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.schoolapp.engine.SchoolsServiceProvider;

public class App {
    public static void main(String[] args) {
        SchoolsServiceProvider ssp = new SchoolsServiceProvider();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        System.out.println("########################################");
        System.out.println("Schools Application - JSON Serialization");
        System.out.println("########################################");
        System.out.println("");

        System.out.println("Printing object before serialization...");
        System.out.println("----------------");
        System.out.println(ssp);
        System.out.println("");

        System.out.println("Printing object after serialization...");
        System.out.println("----------------");
        String json = gson.toJson(ssp);
        System.out.println(json);
        System.out.println("");

        System.out.println("Printing object after de-serialization...");
        System.out.println("----------------");
        System.out.println(gson.fromJson(json, SchoolsServiceProvider.class));
        System.out.println("");

        System.out.println("Application Terminating! ;)");
    }
}