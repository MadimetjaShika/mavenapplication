package com.schoolapp.engine;

import org.fluttercode.datafactory.impl.DataFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

/**
 * Created by Madimetja on 2015/10/18.
 */
class Subject {
    private String title;
    private double average;
    private HashMap<String, Double> students = new HashMap<String, Double>();

    public Subject(String _title) {
        title = _title;
        average = 0.00;
        setStudentMarks();
        calculateAndSetAverage();
    }

    public String getTitle() {
        return title;
    }

    public double getAverage() {
        return average;
    }

    public HashMap<String, Double> getStudentMarks() {
        return students;
    }

    private void setStudentMarks() {
        DataFactory df = new DataFactory();
        //Get a random count of students between 10 and 20
        Random rand = new Random();
        int studentCount = rand.nextInt(10 - 2) + 1;
        double studentPercentage;
        double tempDecimal;

        //Set the student names and assign random grades to the students
        for (int i = 0; i < studentCount; i++) {
            tempDecimal = ((double) rand.nextInt(99 - 1)) / 100;
            studentPercentage = (double) rand.nextInt(99 - 1) + tempDecimal;
            students.put(df.getFirstName() + " " + df.getLastName(), studentPercentage);
        }
    }

    private void calculateAndSetAverage() {
        int studentCount = students.size();
        double total = 0.00;

        Iterator itr = students.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry student = (Map.Entry) itr.next();
            total += (Double) student.getValue();
        }

        this.average = total / (double) studentCount;
    }

    @Override
    public String toString() {
        return "Subject [title=" + title + ", average=" + average +
                ", students=" + students + "]";
    }
}