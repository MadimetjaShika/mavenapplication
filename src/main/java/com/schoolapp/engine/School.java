package com.schoolapp.engine;

import com.schoolapp.exceptions.CustomFileHandlingException;
import com.schoolapp.parser.FileParser;

import java.util.ArrayList;

/**
 * Created by Madimetja on 2015/10/18.
 */
class School {
    private String name;
    ArrayList<Subject> subjects = new ArrayList();

    public School(String _name) {
        this.name = _name;
        try {
            this.prepareSubjects();
        } catch (CustomFileHandlingException e) {
            System.err.println("File Handling Exception was thrown. Subjects could not be prepared. Application will terminate.");
            System.exit(1);
        }
    }

    private void prepareSubjects() throws CustomFileHandlingException {
        FileParser parser = new FileParser();

        ArrayList subjectNames = parser.parseFileByLine("subject_list.txt");

        if (subjectNames != null) {
            for (Object obj : subjectNames) {
                subjects.add(new Subject((String) obj));
            }
        } else
            throw new CustomFileHandlingException("The \"subject_list.txt\" file could not be located. " +
                    "Please ensure the file is placed in the application's working directory.");
    }

    @Override
    public String toString() {
        return "School [name=" + name + ", subjects=" + subjects + "]";
    }
}