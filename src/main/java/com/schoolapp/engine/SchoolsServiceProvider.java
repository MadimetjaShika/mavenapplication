package com.schoolapp.engine;

import org.fluttercode.datafactory.impl.DataFactory;

import java.util.ArrayList;

/**
 * Created by Madimetja on 2015/10/18.
 */
public class SchoolsServiceProvider {
    private ArrayList<School> schools = new ArrayList<School>();

    public SchoolsServiceProvider() {
        this.prepareSchools();
    }

    private void prepareSchools() {
        DataFactory df = new DataFactory();

        for (int i = 0; i < 10; i++) {
            schools.add(new School(df.getLastName() + " High"));
        }
    }

    public ArrayList<School> getSchools() {
        return schools;
    }

    public String toString() {
        return "SchoolsServiceProvider [schools=" + schools + "]";
    }
}